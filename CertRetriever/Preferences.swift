//
//  Preferences.swift
//  CertRetriever
//
//  Created by Admin on 5/8/17.
//  Copyright © 2017 NoMAD. All rights reserved.
//

import Foundation

/// A convenience name for `UserDefaults.standard`
let defaults = UserDefaults.standard

/// The preference keys for the NoMAD defaults domain.
///
/// Use these keys, rather than raw strings.
enum Preferences {

    static let lastCertificateExpiration = "LastCertificateExpiration"
}
